import React,{useState, useEffect} from 'react';
import 'regenerator-runtime/runtime';
import axios from 'axios';

const MainTable = () => {

    const [list, setList] = useState([]);
    const apiUrl = "https://aakarshprakash.in/live/demo.json";
    const fetchDetails = async()=>{
        const {data} = await axios.get(apiUrl);
        const details = data.results;
        setList(details);
    }
    useEffect(()=>{
        fetchDetails();
    },[])
    return (
        <table className="table table-bordered">
            <thead>
            <th>id</th>
            <th>name</th>
            <th>Parent Category</th>
            <th>Is Active</th>
            </thead>
            <tbody>

            { list.length>0 ? (list.map(item => (
                <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{ item?.parentCategory }</td>
                    <td>{item.isActive?"Active":"Not Active"}</td>
                </tr>
            ))):(<tr>NO Data</tr>)}
            </tbody>
        </table>
    );
}

export default MainTable;
