import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import MainTable from "./MainTable";

import styled from 'styled-components'
import {useTable} from 'react-table'

import {userActions} from '../_actions';

class HomePage extends React.Component {
    componentDidMount() {
        this.props.getUsers();
    }

    handleDeleteUser(id) {
        return (e) => this.props.deleteUser(id);
    }

    render() {

        return (
            <div className="col-md-6 col-md-offset-3">

                <h3>Data Table</h3>

                <MainTable/>

                <p>
                    <Link to="/login">Logout</Link>
                </p>
            </div>
        );
    }
}

function mapState(state) {
    const {users, authentication} = state;
    const {user} = authentication;
    return {user, users};
}

const actionCreators = {
    getUsers: userActions.getAll,
    deleteUser: userActions.delete
}

const connectedHomePage = connect(mapState, actionCreators)(HomePage);
export {connectedHomePage as HomePage};